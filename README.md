npm install

disini saya menggunakan api dari pokedox
https://pokeapi.co/api/v2/pokemon/:id

## POKEDOX

untuk menambahkan data barang
body : {
"id_pokemon" :
}
method: POST
url : {{host}}/barang
note: disini saya menambahkan barang dengan mengambil data hasil dari response api
nama diambil dari name, harga dari id yg saya kalikan 10000 dan stock dari base_experience

untuk edit barang
body : {
"nama": "",
"harga": ,
"stock":
}
method : PUT
url : {{host}}/barang/:id

untuk get semua barang
method : GET
url : {{host}}/barangs/

untuk get barang berdasarkan id
method : GET
url : {{host}}/barang/:id

untuk hapus barang
method : DELETE
url : {{host}}/barang/:id

untuk menambahkan data penjualan
body : {
"id_barang" : 2,
"kuantitas" : 2,
"harga" : 20000,
"id_user" : 1,
"status" : 1
}
method : POST
url : {{host}}/penjualan
note : disini jika melakukan penjualan maka stock ditable barang akan langsung berkurang sesuai dengan kuantitas yg inputkan
dan jika stock barang kosong maka tidak akan dimasukan ke table penjualan

get data penjualan
method : GET
url : {{host}}/penjualans
note:disini data yg ditampilkan yg statusnya sama dengan 1

untuk cancel penjualan
method: PUT
url:{{host}}/cancel-penjualan/:id
note:disini akan mengembalikan stock barang sesuai dengan data penjualannya dan status akan jadi 0

untuk database dan ERD sudah saya lampirkan
