const Barang = require('../models/barang.model.js');
const fetch = require('node-fetch');

const getPokemon = async id => {
  const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
  const res = await fetch(url);
  return await res.json();
};
exports.create = async (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: 'Content can not be empty!'
    });
  }
  const data = await getPokemon(req.body.id_pokemon);

  //create barang
  const barang = new Barang({
    nama: data.name,
    harga: `${data.id}` * 10000,
    stock: data.base_experience
  });

  Barang.create(barang, (err, result) => {
    if (err)
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Barang.'
      });
    else res.send(result);
  });
};

exports.findAll = (req, res) => {
  Barang.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving barangs.'
      });
    else res.send(data);
  });
};

exports.findOne = (req, res) => {
  Barang.findById(req.params.barangId, (err, data) => {
    if (err) {
      if (err.kind === 'not_found') {
        res.status(404).send({
          message: `Not found barang with id ${req.params.barangId}.`
        });
      } else {
        res.status(500).send({
          message: 'Error retrieving barang with id ' + req.params.barangId
        });
      }
    } else res.send(data);
  });
};

exports.update = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: 'Content can not be empty!'
    });
  }

  Barang.updateById(req.params.barangId, new Barang(req.body), (err, data) => {
    if (err) {
      if (err.kind === 'not_found') {
        res.status(404).send({
          message: `Not found barang with id ${req.params.barangId}.`
        });
      } else {
        res.status(500).send({
          message: 'Error updating barang with id ' + req.params.barangId
        });
      }
    } else res.send(data);
  });
};

exports.delete = (req, res) => {
  Barang.remove(req.params.barangId, (err, data) => {
    if (err) {
      if (err.kind === 'not_found') {
        res.status(404).send({
          message: `Not found barang with id ${req.params.barangId}.`
        });
      } else {
        res.status(500).send({
          message: 'Could not delete barang with id ' + req.params.barangId
        });
      }
    } else res.send({ message: `barang was deleted successfully!` });
  });
};
