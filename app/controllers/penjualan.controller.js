const Penjualan = require('../models/penjualan.model.js');
const Barang = require('../models/barang.model.js');

exports.create = async (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: 'Content can not be empty!'
    });
  }

  //create barang
  const penjualan = new Penjualan({
    id_barang: req.body.id_barang,
    kuantitas: req.body.kuantitas,
    harga: req.body.harga,
    id_user: req.body.id_user,
    status: req.body.status
  });
  Barang.getStock(req.body.id_barang, (err, jumlah) => {
    if (err) message: err.message;
    else {
      if (jumlah.stock != 0)
        Penjualan.create(penjualan, (err, result) => {
          if (err)
            res.status(500).send({
              message: err.message || 'Some error occurred while creating the Barang.'
            });
          else {
            let datastock = 0;
            Barang.getStock(result.id_barang, (err, data) => {
              if (err) message: err.message;
              datastock = data.stock - result.kuantitas;
              Barang.updateStock(result.id_barang, datastock, (err, response) => {
                if (err) message: err.message;
                else res.send(result);
              });
            });
          }
        });
      else res.send('stock tidak tersedia');
    }
  });
};

exports.findAll = (req, res) => {
  Penjualan.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving barangs.'
      });
    else res.send(data);
  });
};

exports.findOne = (req, res) => {
  Penjualan.findById(req.params.penjualanId, (err, data) => {
    if (err) {
      if (err.kind === 'not_found') {
        res.status(404).send({
          message: `Not found barang with id ${req.params.penjualanId}.`
        });
      } else {
        res.status(500).send({
          message: 'Error retrieving barang with id ' + req.params.penjualanId
        });
      }
    } else res.send(data);
  });
};

exports.update = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: 'Content can not be empty!'
    });
  }

  Penjualan.updateById(req.params.penjualanId, new Penjualan(req.body), (err, data) => {
    if (err) {
      if (err.kind === 'not_found') {
        res.status(404).send({
          message: `Not found barang with id ${req.params.penjualanId}.`
        });
      } else {
        res.status(500).send({
          message: 'Error updating barang with id ' + req.params.penjualanId
        });
      }
    } else res.send(data);
  });
};

exports.cancel = (req, res) => {
  Penjualan.findById(req.params.penjualanId, (err, result) => {
    if (err) {
      if (err.kind === 'not_found') {
        res.status(404).send({
          message: `Not found barang with id ${req.params.penjualanId}.`
        });
      } else {
        res.status(500).send({
          message: 'Error updating barang with id ' + req.params.penjualanId
        });
      }
    } else {
      if (result.status != 0) {
        Penjualan.cancel(result.id, (err, resp) => {
          if (err) {
            message: err.message;
          } else {
            Barang.getStock(result.id_barang, (err, data) => {
              if (err) message: err.message;
              else {
                datastock = data.stock + result.kuantitas;
                Barang.updateStock(result.id_barang, datastock, (err, response) => {
                  if (err) message: err.message;
                  else res.send('penjualan berhasil dicancel');
                });
              }
            });
          }
        });
      } else {
        res.send('penjualan telah dicancel');
      }
    }
  });
};
