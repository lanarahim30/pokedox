module.exports = app => {
  const barang = require('../controllers/barang.controller.js');
  const penjualan = require('../controllers/penjualan.controller.js');

  app.post('/barang', barang.create);

  app.get('/barangs', barang.findAll);

  app.get('/barang/:barangId', barang.findOne);

  app.put('/barang/:barangId', barang.update);

  app.delete('/barang/:barangId', barang.delete);

  app.post('/penjualan', penjualan.create);
  app.get('/penjualans', penjualan.findAll);
  app.put('/cancel-penjualan/:penjualanId', penjualan.cancel);
  // app.delete('/barangdel', barang.deleteAll);
};
