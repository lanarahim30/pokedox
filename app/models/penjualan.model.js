const sql = require('./db.js');

// constructor
class Penjualan {
  constructor(penjualan) {
    this.id_barang = penjualan.id_barang;
    this.kuantitas = penjualan.kuantitas;
    this.harga = penjualan.harga;
    this.id_user = penjualan.id_user;
    this.status = penjualan.status;
  }
  static create(newPenjualan, result) {
    sql.query('INSERT INTO penjualan SET ?', newPenjualan, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      console.log('created penjualan: ', { id: res.insertId, ...newPenjualan });
      result(null, { id: res.insertId, ...newPenjualan });
    });
  }
  static findById(penjualanId, result) {
    sql.query(`SELECT * FROM penjualan WHERE id = ${penjualanId}`, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log('found barang: ', res[0]);
        result(null, res[0]);
        return;
      }

      result({ kind: 'not_found' }, null);
    });
  }
  static getAll(result) {
    sql.query('SELECT * FROM penjualan where status = 1', (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      console.log('barang: ', res);
      result(null, res);
    });
  }
  static updateById(id, barang, result) {
    sql.query('UPDATE penjualan SET nama = ?, harga = ?, stock = ? WHERE id = ?', [barang.nama, barang.harga, barang.stock, id], (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: 'not_found' }, null);
        return;
      }

      console.log('updated barang: ', { id: id, ...barang });
      result(null, { id: id, ...barang });
    });
  }
  static cancel(id, result) {
    sql.query('UPDATE penjualan SET status = 0 WHERE id = ?', [id], (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: 'not_found' }, null);
        return;
      }

      console.log('updated barang: ', { id: id, ...id });
      result(null, { id: id, ...id });
    });
  }
}

module.exports = Penjualan;
