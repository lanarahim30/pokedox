const sql = require('./db.js');

// constructor
class Barang {
  constructor(barang) {
    this.nama = barang.nama;
    this.harga = barang.harga;
    this.stock = barang.stock;
  }
  static create(newBarang, result) {
    sql.query('INSERT INTO barang SET ?', newBarang, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      console.log('created barang: ', { id: res.insertId, ...newBarang });
      result(null, { id: res.insertId, ...newBarang });
    });
  }
  static findById(barangId, result) {
    sql.query(`SELECT * FROM barang WHERE id = ${barangId}`, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log('found barang: ', res[0]);
        result(null, res[0]);
        return;
      }

      result({ kind: 'not_found' }, null);
    });
  }
  static getAll(result) {
    sql.query('SELECT * FROM barang', (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      console.log('barang: ', res);
      result(null, res);
    });
  }
  static getStock(id, result) {
    sql.query(`SELECT stock FROM barang where id = ${id}`, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log('found barang: ', res[0]);
        result(null, res[0]);
        return;
      }

      result({ kind: 'not_found' }, null);
    });
  }
  static updateById(id, barang, result) {
    sql.query('UPDATE barang SET nama = ?, harga = ?, stock = ? WHERE id = ?', [barang.nama, barang.harga, barang.stock, id], (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: 'not_found' }, null);
        return;
      }

      console.log('updated barang: ', { id: id, ...barang });
      result(null, { id: id, ...barang });
    });
  }
  static updateStock(id, stock, result) {
    sql.query('UPDATE barang set stock = ? where id = ?', [stock, id], (err, res) => {
      // console.log(res);
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: 'not_found' }, null);
        return;
      }

      console.log('updated barang: ', { id: id, ...stock });
      result(null, { id: id, ...stock });
    });
  }
  static remove(id, result) {
    sql.query('DELETE FROM barang WHERE id = ?', id, (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: 'not_found' }, null);
        return;
      }

      console.log('deleted barang with id: ', id);
      result(null, res);
    });
  }
  static removeAll(result) {
    sql.query('DELETE FROM barang', (err, res) => {
      if (err) {
        console.log('error: ', err);
        result(null, err);
        return;
      }

      console.log(`deleted ${res.affectedRows} barang`);
      result(null, res);
    });
  }
}

module.exports = Barang;
